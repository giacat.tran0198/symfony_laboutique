<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CartController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/mon-panier", name="cart")
     * @param Cart $cart
     * @return Response
     */
    public function index(Cart $cart): Response
    {
        return $this->render('cart/index.html.twig', [
            'cart' => $cart->getFull(),
        ]);
    }

    /**
     * @Route("/mon-panier/ajouter/{id}", name="add_to_cart")
     * @param int $id
     * @param Cart $cart
     * @return Response
     */
    public function add(int $id, Cart $cart)
    {
        $cart->add($id);
        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/mon-panier/retire", name="remove_my_cart")
     * @param Cart $cart
     * @return Response
     */
    public function remove(Cart $cart)
    {
        $cart->remove();
        return $this->redirectToRoute('products');
    }

    /**
     * @Route("/mon-panier/supprmier/{id}", name="delete_to_cart")
     * @param int $id
     * @param Cart $cart
     * @return Response
     */
    public function delete(int $id, Cart $cart)
    {
        $cart->delete($id);
        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/mon-panier/diminuer/{id}", name="decrease_to_cart")
     * @param int $id
     * @param Cart $cart
     * @return Response
     */
    public function decrease(int $id, Cart $cart)
    {
        $cart->decrease($id);
        return $this->redirectToRoute('cart');
    }
}
