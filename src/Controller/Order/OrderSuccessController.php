<?php

namespace App\Controller\Order;

use App\Classe\Cart;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderSuccessController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderValidateController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/commande/merci/{stripeSessionId}", name="order_validate")
     * @param string $stripeSessionId
     * @param Cart $cart
     * @return Response
     */
    public function index(string $stripeSessionId, Cart $cart): Response
    {
        $order = $this->entityManager->getRepository(Order::class)->findOneByStripeSessionId($stripeSessionId);
        if (!$order || $order->getUser() != $this->getUser() || !$order instanceof Order) {
            return $this->redirectToRoute('home');
        }

        if (!$order->getState() == 0){
            $cart->remove();

            $order->setState(1);
            $this->entityManager->flush();
        }

        return $this->render('order/order_success/index.html.twig', [
            'order' => $order
        ]);
    }
}
